Code for F# AST viewer page [http://ratatosk.dynu.net/ast/](http://ratatosk.dynu.net/ast/).

## Pre-requisites

You'll need to install the following pre-requisites in order to build SAFE applications

* The .NET Core SDK 2.1.
* FAKE 5 installed as a global tool
* The Yarn package manager.
* Node 8.x installed for the front end components.
* If you're running on OSX or Linux, you'll also need to install Mono.

## Building

Run `fake build --target run` to build and run the app. After a short delay, you'll be presented with a basic SAFE application running in your browser. The application will by default run in "development mode", which means it automatically watch your project for changes; whenever you save a file in the client project it will refresh the browser automatically; if you save a file in the server project it will also restart the server in the background.

### Using Docker

You can build docker image of app with `docker build -f build.Dockerfile .`

Latest docker image of this app can be found in `registry.gitlab.com/jindraivanek/ast-viewer:master`. You can run it locally on port 8085 with `docker run -p 8085:8085 registry.gitlab.com/jindraivanek/ast-viewer:master`.

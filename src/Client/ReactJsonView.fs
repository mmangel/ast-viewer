module ReactJsonView

open Fable.Core.JsInterop
open Fable.Import
open Fable.Core
open Fable.Helpers.React

module JsonViewer =

    type Props =
        | Src of obj
        | Name of string
        | DisplayObjectSize of bool
        | DisplayDataTypes of bool
        | IndentWidth of int
        | ShouldCollapse of obj

    let inline viewer (props: Props list) : React.ReactElement =
        ofImport "default" "react-json-view" (keyValueList CaseRules.LowerFirst props) []
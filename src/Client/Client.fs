module Client

open Elmish
open Elmish.React

open Fable.Core.JsInterop
open Fable.Helpers.React
open Fable.Helpers.React.Props
open Shared
open Fulma
open Fulma.FontAwesome
open ReactEditor
open ReactJsonView
open Fable.PowerPack
open Fable.PowerPack.Fetch


importSideEffects "./sass/style.sass"
module Result =
    let toOption = function
        | Ok x -> Some x
        | Error _ -> None

module URI =
    let private compressToEncodedURIComponent(_x: string): string  = importMember "./js/util.js"
    let private decompressFromEncodedURIComponent(_x: string): string  = importMember "./js/util.js"
    let private getURIhash(): string  = importMember "./js/util.js"
    let private setURIhash(_x: string): unit  = importMember "./js/util.js"

    let parseQuery() =
        getURIhash().Split[|'&'|]
        |> Seq.choose (fun s ->
            match s.Split[|'='|] |> Seq.toList with
            | [key; value] -> Some (key, decompressFromEncodedURIComponent value)
            | _ -> None)
        |> Map.ofSeq

    let updateQuery m =
        m |> Map.toSeq |> Seq.map (fun (key, value) -> key + "=" + compressToEncodedURIComponent value) |> String.concat "&"
        |> setURIhash

// The Msg type defines what events/actions can occur while the application is running
// the state of the application changes *only* in reaction to these events
type Msg =
| VersionFound of string
| SetSourceText of string
| DoParse
| DoTypeCheck
| Parsed of Dto
| TypeChecked of Dto
| Error of string
| ShowJsonViewer
| ShowEditor
| ShowRaw

module Server =

    open Fable.Remoting.Client

    /// A proxy you can use to talk to server directly
    let api : IModelApi =
      Remoting.createApi()
      |> Remoting.withRouteBuilder Route.builder
      |> Remoting.buildProxy<IModelApi>

// defines the initial state and initial command (= side-effect) of the application
let init () : Model * Cmd<Msg> =
    let initialModel = Model.Default
    let cmd = Cmd.ofAsync Server.api.version () VersionFound (fun ex -> Error ex.Message)
    let query = URI.parseQuery()
    let code = query |> Map.tryFind "code" |> Option.defaultValue ""
    { initialModel with Source = code}, Cmd.batch [cmd; Cmd.ofMsg (DoParse)]

//HACK: workaround for some Fable.Remoting problems I don't have time to investigate
//TODO: figure out what's wrong with Fable.Remoting deserialization
let fetchParse (body: string) =
    let body = sprintf "[%s]" (Fable.Import.JS.JSON.stringify body)
    promise {
        let props =
            [ RequestProperties.Method HttpMethod.POST
              Fetch.requestHeaders [
                HttpRequestHeaders.ContentType "application/json" ]
              RequestProperties.Body !^body ]
        let! res = Fetch.fetch "api/ast/IModelApi/parse" props
        let! r = res.json()
        return r?Ok
    }

//HACK: workaround for some Fable.Remoting problems I don't have time to investigate
//TODO: figure out what's wrong with Fable.Remoting deserialization
let fetchTypeCheck (body: string) =
    let body = sprintf "[%s]" (Fable.Import.JS.JSON.stringify body)
    promise {
        let props =
            [ RequestProperties.Method HttpMethod.POST
              Fetch.requestHeaders [
                HttpRequestHeaders.ContentType "application/json" ]
              RequestProperties.Body !^body ]
        let! res = Fetch.fetch "api/ast/IModelApi/typeCheck" props
        let! r = res.json()
        return r?Ok
    }

// The update function computes the next state of the application based on the current state and the incoming events/messages
// It can also run side-effects (encoded as commands) like calling the server via Http.
// these commands in turn, can dispatch messages to which the update function will react.
let update (msg : Msg) (currentModel : Model) : Model * Cmd<Msg> =
    match msg with
    | SetSourceText x ->
        let nextModel = { currentModel with Source = x }
        nextModel, Cmd.none
    | Parsed x ->
        let nextModel = { currentModel with IsLoading = false; Parsed = Some x }
        nextModel, Cmd.none
    | TypeChecked x ->
        let nextModel = { currentModel with IsLoading = false; Parsed = Some x }
        nextModel, Cmd.none
    | Error _ ->
        let nextModel = { currentModel with IsLoading = false; Parsed = None }
        nextModel, Cmd.none
    | DoParse ->
        URI.updateQuery (Map.ofSeq ["code", currentModel.Source])
        let response =
            Cmd.ofPromise
                (fun x ->  fetchParse x)
                currentModel.Source
                Parsed
                (fun e -> Error (e.Message + "\n" + e.StackTrace))
        { currentModel with IsLoading = true }, response
    | DoTypeCheck ->
        URI.updateQuery (Map.ofSeq ["code", currentModel.Source])
        let response =
            Cmd.ofPromise
                (fun x -> fetchTypeCheck x)
                currentModel.Source
                TypeChecked
                (fun e -> Error (e.Message + "\n" + e.StackTrace))
        { currentModel with IsLoading = true }, response
    | VersionFound version -> { currentModel with Version = version }, Cmd.none
    | ShowJsonViewer -> {currentModel with View = Shared.JsonViewer}, Cmd.none
    | ShowEditor -> {currentModel with View = Shared.Editor}, Cmd.none
    | ShowRaw -> {currentModel with View = Shared.Raw}, Cmd.none

let safeComponents =
    let astViewerlinks =
        span [ ]
           [
             a [ Href "https://gitlab.com/jindraivanek/ast-viewer" ] [ str "source code" ]
             str ", "
             a [ Href "https://gitlab.com/jindraivanek/ast-viewer/issues/new" ] [ str "create issue" ]
           ]

    [ p [ ]
        [ strong [] [ str "AST-viewer " ]
          astViewerlinks ]
 ]

let button disabled txt onClick =
    Button.button
        [ Button.IsFullWidth
          Button.Color IsPrimary
          Button.OnClick onClick
          Button.Disabled disabled ]
        [ str txt ]



let viewNavbar version =
        Navbar.navbar [ Navbar.IsFixedTop ]
                [ Navbar.Brand.div [ ]
                    [ Navbar.Item.a [ ]
                        [ strong [ ]
                            [ sprintf "F# AST viewer - FSC version v%s" version |> str ] ] ]
                  Navbar.End.div [ ]
                    [ Navbar.Item.div [ ]
                        [ Button.a [ Button.Props [ Href "https://gitlab.com/jindraivanek/ast-viewer" ]
                                     Button.Color IsWarning ]
                            [ Icon.faIcon [ ]
                                [ Fa.icon Fa.I.Gitlab ]
                              span [ ]
                                [ str "Gitlab" ] ] ] ] ]

let sourceAndFormatted model dispatch sourceTooBig =

    let headers =
        Columns.columns [ Columns.IsGapless ; Columns.IsMultiline ; Columns.CustomClass "is-gapless" ]
                        [ Column.column [ Column.Width(Screen.All, Column.IsHalf) ]
                            [ Message.message [ Message.Props [Id "input-message"] ]
                                [ Message.body [ ]
                                    [ Text.div [ Modifiers [ Modifier.TextAlignment (Screen.All, TextAlignment.Centered) ] ]
                                        [ str "Type or paste F# code" ] ] ] ]
                          Column.column [ Column.Width(Screen.All, Column.IsHalf) ]
                            [ Message.message [ Message.Props [Id "formatted-message"] ]
                                [ Message.body [ ]
                                    [ Text.div [ Modifiers [ Modifier.TextAlignment (Screen.All, TextAlignment.Centered) ] ]
                                        [ str "F# AST" ] ] ] ] ]

    let editorButtons =
        let button txt onClick =
            Button.button
                [ Button.IsFullWidth
                  Button.Color IsInfo
                  Button.OnClick onClick ]
                [ str txt ]

        Columns.columns []
                    [ Column.column [ Column.Width(Screen.All, Column.Is4) ]
                        [button "Show JsonViewer" (fun ev -> dispatch (ShowJsonViewer))]
                      Column.column [ Column.Width(Screen.All, Column.Is4) ]
                        [button "Show editor" (fun ev -> dispatch (ShowEditor))]
                      Column.column [ Column.Width(Screen.All, Column.Is4) ]
                        [button "Show raw" (fun ev -> dispatch (ShowRaw))]
                    ]

    let editors =
        Columns.columns [ Columns.IsGapless ; Columns.IsMultiline ; Columns.CustomClass "is-gapless" ]
            [
              Column.column [] [
                Editor.editor [ Editor.Language "fsharp"
                                Editor.IsReadOnly false
                                Editor.Value model.Source
                                Editor.OnChange (SetSourceText >> dispatch) ]
                (if sourceTooBig then Notification.notification [ Notification.Color IsDanger ] [str (sprintf "Source code size is limited to 10 kB.")] else div [] []) ]
              Column.column [] [
                  Control.div [Control.IsLoading model.IsLoading; Control.CustomClass "is-large";] [
                        yield editorButtons
                        yield
                            match model.View with
                            | Shared.Raw ->
                                Editor.editor [ Editor.Language "fsharp"
                                                Editor.IsReadOnly true
                                                Editor.Value model.Parsed.Value.String ]
                            | Shared.Editor ->
                                Editor.editor [ Editor.Language "fsharp"
                                                Editor.IsReadOnly true
                                                Editor.Value (Fable.Import.JS.JSON.stringify(model.Parsed.Value.Node, !!null, 2) ) ]
                            | Shared.JsonViewer ->
                                Control.div [Control.CustomClass "viewer"] [
                                    JsonViewer.viewer [ JsonViewer.Src model.Parsed.Value.Node
                                                        JsonViewer.Name null
                                                        JsonViewer.DisplayDataTypes false
                                                        JsonViewer.DisplayObjectSize false
                                                        JsonViewer.IndentWidth 2
                                                        JsonViewer.ShouldCollapse (fun x -> x?name = "Range")]
                                ]
                     ] ]
            ]

    [ headers ; editors ]

let buttonsView sourceTooBig (model : Model) (dispatch : Msg -> unit)=
    Columns.columns []
                    [ Column.column [ Column.Width(Screen.All, Column.IsHalf) ]
                        [button sourceTooBig (if model.IsLoading then "Working..." else "Show Untyped AST") (fun ev -> dispatch (DoParse))]
                      Column.column [ Column.Width(Screen.All, Column.IsHalf) ]
                        [button sourceTooBig (if model.IsLoading then "Working..." else "Show Typed AST") (fun ev -> dispatch (DoTypeCheck))]
                    ]


let footer =
    Footer.footer [ ]
                  [ Content.content [ Content.Modifiers [ Modifier.TextAlignment (Screen.All, TextAlignment.Centered) ] ] safeComponents ]

let view (model : Model) (dispatch : Msg -> unit) =
    let sourceTooBig = model.Source.Length > Const.sourceSizeLimit

    div []
        [ viewNavbar model.Version
          div [ Class "page-content"]
                  [
                    yield! sourceAndFormatted model dispatch sourceTooBig
                    yield buttonsView sourceTooBig model dispatch
                    yield footer
                  ]
        ]


#if DEBUG
open Elmish.Debug
open Elmish.HMR
#endif

Program.mkProgram init update view
#if DEBUG
|> Program.withConsoleTrace
|> Program.withHMR
#endif
|> Program.withReact "elmish-app"
#if DEBUG
|> Program.withDebugger
#endif
|> Program.run

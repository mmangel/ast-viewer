open System.IO
open System.Threading.Tasks

open FSharp.Control.Tasks.V2
open Saturn
open Shared

open Fable.Remoting.Server
open Fable.Remoting.Giraffe

open Microsoft.FSharp.Compiler.SourceCodeServices


let publicPath = Path.GetFullPath "../Client/public"
let port = 8085us

module Result =
    let attempt f =
        try
            Ok <| f()
        with e -> Error e

module Reflection =
    open FSharp.Reflection
    let mapRecordToType<'t> (x:obj) =
        let values = FSharpValue.GetRecordFields x
        FSharpValue.MakeRecord(typeof<'t>, values) :?> 't

let lck = obj()

let getFscVersion () =
    let assembly = typeof<Microsoft.FSharp.Compiler.SourceCodeServices.FSharpChecker>.Assembly
    let version = assembly.GetName().Version
    sprintf "%i.%i.%i" version.Major version.Minor version.Revision
    |> fun v -> async { return v }


let init() : Task<Model> = task { return Model.Default }

let sharedChecker = lazy(FSharpChecker.Create(keepAssemblyContents = true))

let getProjectOptionsFromScript file source (checker : FSharpChecker) = async {
    let targetFramework = Environment.netReferecesAssembliesTFMLatest ()
    let additionaRefs =
        Environment.additionalArgumentsBy targetFramework
        |> Array.ofList

    let! (opts, _) = checker.GetProjectOptionsFromScript(file, source, otherFlags = additionaRefs, assumeDotNetFramework = true)
    return opts
}

let parse (source: string) =
    let fileName = "tmp.fsx"
    // Create an interactive checker instance (ignore notifications)
    let checker = sharedChecker.Value
    // Get compiler options for a single script file
    let checkOptions =
        checker
        |> getProjectOptionsFromScript fileName source
        |> (Async.RunSynchronously >> checker.GetParsingOptionsFromProjectOptions >> fst)
    // Run the first phase (untyped parsing) of the compiler
    let untypedRes = checker.ParseFile(fileName, source, checkOptions) |> Async.RunSynchronously
    if untypedRes.ParseHadErrors then
        let errors =
            untypedRes.Errors
            |> Array.filter (fun e -> e.Severity = FSharpErrorSeverity.Error)
        if not <| Array.isEmpty errors then
            failwithf "Parsing failed with errors: %A\nAnd options: %A" errors checkOptions

    match untypedRes.ParseTree with
    | Some (Microsoft.FSharp.Compiler.Ast.ParsedInput.ImplFile(Microsoft.FSharp.Compiler.Ast.ParsedImplFileInput(_,_,_,_,_, [Microsoft.FSharp.Compiler.Ast.SynModuleOrNamespace(_,_,_, bindings, _, _ , _,_)], _))) ->
        bindings
    | _ -> failwithf "Parsing failed. Please select a complete code fragment to format."

let typeCheck (source: string) =
    let fileName =  "tmp.fsx"
    let checker = sharedChecker.Value
    let options =
        checker
        |> getProjectOptionsFromScript fileName source
        |> Async.RunSynchronously
    let parseRes, typedRes = checker.ParseAndCheckFileInProject(fileName, 1, source, options) |> Async.RunSynchronously
    match typedRes with
    | FSharpCheckFileAnswer.Aborted ->

        failwithf "Type checking aborted. With Parse errors:\n%A\n And with options: \n%A" parseRes.Errors options
    | FSharpCheckFileAnswer.Succeeded res ->
        match res.ImplementationFile with
        | None -> failwith "Some error"
        | Some fc -> fc.Declarations


let parseTask (x: string) =
    task {
        return
            if x.Length > Const.sourceSizeLimit then Error "Source size limit exceeded." else
            lock lck (fun () ->
                Result.attempt <| fun () ->
                    let ast = parse x
                    let node = AstTransformer.astToNode (x.Split '\n' |> Array.length) ast
                    {Node = node; String = sprintf "%A" ast}
                |> Result.mapError (fun exn -> sprintf "Exception: %s Stack Trace: %s" exn.Message exn.StackTrace)) }

let typeCheckTask (x: string) =
    task {
        return
            if x.Length > Const.sourceSizeLimit then Error "Source size limit exceeded." else
            lock lck (fun () ->
                Result.attempt <| fun () ->
                    let tast = typeCheck x
                    let node = TastTransformer.tastToNode tast
                    {Node = node; String = sprintf "%A" tast}
                |> Result.mapError (fun exn -> sprintf "Exception: %s Stack Trace: %s" exn.Message exn.StackTrace)) }


let modelApi = {
    init = init >> Async.AwaitTask
    parse = parseTask >> Async.AwaitTask
    version = getFscVersion
    typeCheck = typeCheckTask >> Async.AwaitTask
}

let webApp =
    Remoting.createApi()
    |> Remoting.withRouteBuilder Route.builder
    |> Remoting.fromValue modelApi
    |> Remoting.buildHttpHandler

let app = application {
    url ("http://0.0.0.0:" + port.ToString() + "/")
    use_router webApp
    memory_cache
    use_static publicPath
    use_gzip
}

run app
